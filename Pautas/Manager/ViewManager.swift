//
//  ViewManager.swift
//  Pautas
//
//  Created by José C Feitosa on 18/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import UIKit

class ViewManager {
    class func prepareWindow(window: UIWindow?) {
        let window = window ?? UIWindow()
        window.rootViewController = LoginViewController()
        window.makeKeyAndVisible()
    }
    
    class func esqueciSenhaViewController(vc: UIViewController) {
        let esqueciVC = EsqueciSenhaViewController()
        vc.present(esqueciVC, animated: true, completion: nil)
    }
    
    class func cadastroViewController(vc: UIViewController) {
        let esqueciVC = CadastroViewController()
        if let login = vc as? LoginViewController {
            esqueciVC.delegate = login
        }
        vc.present(esqueciVC, animated: true, completion: nil)
    }
    
    class func cadastroEditViewController(vc: UIViewController) {
        let pautaCad = CadastroEditPautaViewController()
        if let main = vc as? MainViewController {
            pautaCad.delegate = main
        }
        vc.present(pautaCad, animated: true, completion: nil)
    }
    
    class func mainViewController(vc: UIViewController) {
        let mainVC = MainViewController()
        let mainNav = MainNavigationController(rootViewController: mainVC)
        mainNav.modalPresentationStyle = .fullScreen
        vc.present(mainNav, animated: true, completion: nil)
    }
    
    class func alert(with title: String?, message: String?, in viewController: UIViewController, confirmActionHandler: ((_ action: UIAlertAction)->Void)?, cancelAction: ((_ action: UIAlertAction)->Void)?, completion: (()->Void)?) {
        
        func showDefault(title: String?, message: String?, in viewController: UIViewController, confirmActionHandler: ((_ action: UIAlertAction)->Void)?, completion: (()->Void)?){
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let confirmAction = UIAlertAction(title: "OK", style: .default, handler: confirmActionHandler)
            alert.addAction(confirmAction)
            DispatchQueue.main.async{
                viewController.present(alert, animated: true, completion: completion)
            }
        }
        
        func show(with title: String?, message: String?, in viewController: UIViewController, confirmActionHandler: ((_ action: UIAlertAction)->Void)?, cancelAction: ((_ action: UIAlertAction)->Void)?, completion: (()->Void)?){
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let confirmAction = UIAlertAction(title: "Sim", style: .destructive, handler: confirmActionHandler)
            let cancelAction = UIAlertAction(title: "Não", style: .default, handler: cancelAction)
            alert.addAction(confirmAction)
            alert.addAction(cancelAction)
            DispatchQueue.main.async{
                viewController.present(alert, animated: true, completion: completion)
            }
        }
        
        guard let cancelAction = cancelAction else {
            showDefault(title: title, message: message, in: viewController, confirmActionHandler: confirmActionHandler, completion: completion)
            return
        }
        show(with: title, message: message, in: viewController, confirmActionHandler: confirmActionHandler, cancelAction: cancelAction, completion: completion)
    }
    
    class func showAlertOptionsUser (in viewController: UIViewController, firstActionHandler: ((_ action: UIAlertAction)->Void)?, secondActionHandler: ((_ action: UIAlertAction)->Void)?, completion: (()->Void)?) {
    
        let alert = UIAlertController(title: "Ações", message: nil, preferredStyle: .actionSheet)
        let firstOption = UIAlertAction(title: "Detalhes do usuário", style: .default, handler: firstActionHandler)
        let secondOption = UIAlertAction(title: "Sair", style: .destructive, handler: secondActionHandler)
        let cancel = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        secondOption.setValue(UIImage.init(named: "icon_logout"), forKey: "image")
        firstOption.setValue(UIImage.init(named: "icon_user"), forKey: "image")
        
        alert.addAction(firstOption)
        alert.addAction(secondOption)
        alert.addAction(cancel)
        DispatchQueue.main.async {
            viewController.present(alert, animated: true, completion: completion)
        }
    }

}

// ViewController que será mãe de todas as viewcontrollers
class DefaultViewController: UIViewController {
    
    var activityController: UIView? = nil
    var labelActivity: UILabel = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 200.0, height: 30.0))
    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    
    weak var constraintButton: NSLayoutConstraint?
    
    var initialValueOfConstraintButton:CGFloat = 0
    
    let constantes = Constantes()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            self.overrideUserInterfaceStyle = .light
        } else {
        }
    }
    
    func setupNavigation(_ title: String? = nil) {
        self.navigationItem.title = title ?? "Pautas"
        self.navigationItem.titleView?.tintColor = constantes.mainViewStruct.navBarTextColor
        self.navigationItem.leftBarButtonItem?.tintColor = constantes.mainViewStruct.navBarTextColor
        self.navigationItem.rightBarButtonItem?.tintColor = constantes.mainViewStruct.navBarTextColor
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func showActivity(){
        if activityController == nil {
            activityController = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
            activityController?.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.6)
            let activity = UIActivityIndicatorView(style: .whiteLarge)
            activity.center = (activityController?.center)!
            activity.startAnimating()
            
            labelActivity.frame.size.width = (activityController?.bounds.width ?? 0.0) * 0.9
            labelActivity.center = (activityController?.center)!
            labelActivity.frame.origin.x = ((activityController?.bounds.width ?? 0.0) - labelActivity.bounds.width)/2
            labelActivity.frame.origin.y = activity.frame.origin.y + activity.bounds.height + 8
            labelActivity.text = "Carregando..."
            labelActivity.textAlignment = .center
            labelActivity.textColor = UIColor.white
            
            activityController?.addSubview(labelActivity)
            activityController?.addSubview(activity)
        }
        guard let navigationController = self.navigationController else {
            self.view.addSubview(self.activityController!)
            return
        }
        navigationController.view.addSubview(activityController!)
    }
    
    func hideActivity(){
        activityController?.removeFromSuperview()
    }
    
    func registerForKeyboardNotification(initialValueForConstraint: CGFloat){
        self.initialValueOfConstraintButton = initialValueForConstraint
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWasShown),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden), name: Notification.Name("UIKeyboardWillHideNotification"), object: nil)
    }
    
    @objc func keyboardWasShown(_ notification: Notification) {
        if constraintButton != nil {
            if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardRectangle = keyboardFrame.cgRectValue
                let keyboardHeight = keyboardRectangle.height
                constraintButton?.constant = (keyboardHeight + 10) * -1
                self.view.updateConstraints()
            }
        }
    }
    
    @objc func keyboardWillBeHidden() {
        if constraintButton != nil {
            constraintButton?.constant = initialValueOfConstraintButton
            self.view.updateConstraints()
        }
    }
    
}


