//
//  CadastroCellStruct.swift
//  Pautas
//
//  Created by TempDev on 20/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import UIKit

struct CadastroCellStruct {
    let placeholderTextField: String
    let keyboradTypeTextField: UIKeyboardType
    let cornerRadiusTextField: CGFloat
    let borderColorTextField: CGColor
    let borderWidthTextField: CGFloat
    let autoCapitalizeTextField: UITextAutocapitalizationType
    let textColorTextField: UIColor
    let isSecuryEntry: Bool
}
