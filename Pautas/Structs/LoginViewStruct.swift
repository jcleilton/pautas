//
//  LoginViewStruct.swift
//  Pautas
//
//  Created by José C Feitosa on 19/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import UIKit

struct LoginViewStruct {
    let imagemLogo: UIImage
    let imagemSublogo: UIImage
    let backgroundColor: UIColor
    let textFieldTextColor: UIColor
    let textFieldBorderWidth: CGFloat
    let textFieldCornerRadius: CGFloat
    let textFieldBorderColor: CGColor
    let buttonTextColor: UIColor
    let buttonBackgroundColor: UIColor
    let buttonCornerRadius: CGFloat
    let buttonBorderWidth: CGFloat
    let buttonBorderColor: CGColor
    let labelTextColor: UIColor
}
