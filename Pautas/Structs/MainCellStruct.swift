//
//  MainCellStruct.swift
//  Pautas
//
//  Created by José C Feitosa on 20/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import Foundation

struct MainCellStruct {
    var expandida: Bool
    let data: Pauta
}
