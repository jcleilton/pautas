//
//  MainViewStruct.swift
//  Pautas
//
//  Created by José C Feitosa on 20/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import UIKit

struct MainViewStruct {
    let viewBackgroundColor: UIColor
    let navBarBackgroundColor: UIColor
    let navBarTextColor: UIColor
    let labelTextColor: UIColor
    let cadastroCellStruct: CadastroCellStruct
}
