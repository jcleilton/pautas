//
//  EsqueciSenhaViewController.swift
//  Pautas
//
//  Created by José C Feitosa on 19/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import UIKit

class EsqueciSenhaViewController: DefaultViewController {
    lazy var formView: UIView = {
        let obj = UIView()
        obj.backgroundColor = .clear
        obj.translatesAutoresizingMaskIntoConstraints = false
        return obj
    }()
    
    lazy var usuarioTextField: UITextField = { [weak self] in
        guard let me = self else {return UITextField()}
        let obj = UITextField()
        obj.tag = 1
        obj.placeholder = "E-mail"
        obj.keyboardType = .emailAddress
        obj.autocorrectionType = .no
        obj.textAlignment = .center
        obj.layer.cornerRadius = me.constantes.loginStruct.textFieldCornerRadius
        obj.layer.borderColor = me.constantes.loginStruct.textFieldBorderColor
        obj.layer.borderWidth = me.constantes.loginStruct.textFieldBorderWidth
        obj.autocapitalizationType = .none
        obj.textColor = me.constantes.loginStruct.textFieldTextColor
        obj.translatesAutoresizingMaskIntoConstraints = false
        return obj
    }()
    
    lazy var returnButton: UIButton = { [weak self] in
        let obj = UIButton()
        guard let me = self else { return obj }
        let constants = me.constantes
        obj.setImage(constants.voltarButtonImage, for: .normal)
        obj.imageView?.contentMode = .scaleAspectFit
        obj.tintColor = constants.loginStruct.labelTextColor
        obj.translatesAutoresizingMaskIntoConstraints = false
        obj.addTarget(me, action: #selector(me.closeMe), for: .touchUpInside)
        return obj
    }()
    
    lazy var okButton: UIButton = { [weak self] in
        let obj = UIButton()
        guard let me = self else { return obj }
        obj.setTitle("Redefir Senha", for: .normal)
        obj.setTitleColor(me.constantes.loginStruct.buttonTextColor, for: .normal)
        obj.translatesAutoresizingMaskIntoConstraints = false
        obj.layer.cornerRadius = me.constantes.loginStruct.buttonCornerRadius
        obj.layer.borderWidth = me.constantes.loginStruct.buttonBorderWidth
        obj.layer.borderColor = me.constantes.loginStruct.buttonBorderColor
        obj.backgroundColor = me.constantes.loginStruct.buttonBackgroundColor
        if let me = self {
            obj.addTarget(me, action: #selector(me.okAction), for: .touchUpInside)
        }
        return obj
    }()
    
    var viewModel = EsqueciSenhaViewModel()
    
    var bottomValue = CGFloat(0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.delegate = self
        self.setup()
        
    }
    
    func setup() {
        self.view.backgroundColor = self.constantes.loginStruct.backgroundColor
        
        bottomValue = (self.view.bounds.height * 0.4) * -1
        
        self.registerForKeyboardNotification(initialValueForConstraint: bottomValue)
        
        self.view.addSubview(self.formView)
        self.formView.addSubview(self.usuarioTextField)
        self.formView.addSubview(self.okButton)
        
        
        self.constraintButton = formView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: bottomValue)
        self.constraintButton?.isActive = true
        formView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        formView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.85).isActive = true
        formView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.4).isActive = true
        
        self.okButton.bottomAnchor.constraint(equalTo: self.formView.bottomAnchor, constant: -20).isActive = true
        self.okButton.centerXAnchor.constraint(equalTo: self.formView.centerXAnchor).isActive = true
        self.okButton.widthAnchor.constraint(equalToConstant: 200).isActive = true
        self.okButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        self.usuarioTextField.bottomAnchor.constraint(equalTo: self.okButton.topAnchor, constant: -10).isActive = true
        self.usuarioTextField.centerXAnchor.constraint(equalTo: self.okButton.centerXAnchor).isActive = true
        self.usuarioTextField.widthAnchor.constraint(equalToConstant: 200).isActive = true
        self.usuarioTextField.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        self.formView.addSubview(label)
        label.text = "Um e-mail será enviado, contendo uma nova senha, caso exista um usuário cadastrado com o e-mail informado."
        label.numberOfLines = 0
        label.textColor = self.constantes.loginStruct.labelTextColor
        label.textAlignment = .center
        
        
        label.bottomAnchor.constraint(equalTo: self.usuarioTextField.topAnchor, constant: -10).isActive = true
        label.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 30).isActive = true
        label.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -30).isActive = true
        
        
        self.view.addSubview(self.returnButton)
        
        self.returnButton.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true
        self.returnButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        self.returnButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.returnButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        self.view.layoutIfNeeded()
        
        
    }
    
    @objc func closeMe() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func okAction() {
        let email = self.usuarioTextField.text ?? ""
        self.viewModel.esqueciSenhaRequest(email: email)
    }
}

extension EsqueciSenhaViewController: EsqueciSenhaDelegate {
    func finalizandoComErro(message: String) {
        ViewManager.alert(with: "Atenção", message: message, in: self, confirmActionHandler: nil, cancelAction: nil, completion: nil)
    }
    
    func finalizandoComSucesso() {
        ViewManager.alert(with: "Sucesso", message: "Uma nova senha foi mandada para o seu email!", in: self, confirmActionHandler: { [weak self] (_) in
            DispatchQueue.main.async {
                self?.dismiss(animated: true, completion: nil)
            }
        }, cancelAction: nil, completion: nil)
    }
    
    func carregando() {
        self.showActivity()
    }
    
    func finalizarCarregando() {
        self.hideActivity()
    }
    
    
}
