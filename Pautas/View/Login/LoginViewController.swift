//
//  ViewController.swift
//  Pautas
//
//  Created by José C Feitosa on 18/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import UIKit

class LoginViewController: DefaultViewController {
    
    let viewModel: LoginViewModel = LoginViewModel()
    
    lazy var imageViewLogo: UIImageView = { [weak self] in
        guard let me = self else { return UIImageView() }
        let obj = UIImageView()
        obj.image = me.constantes.loginStruct.imagemLogo
        obj.contentMode = .scaleAspectFit
        obj.translatesAutoresizingMaskIntoConstraints = false
        return obj
    }()
    
    lazy var imageViewSubLogo: UIImageView = { [weak self] in
        guard let me = self else { return UIImageView() }
        let obj = UIImageView()
        obj.image = me.constantes.loginStruct.imagemSublogo
        obj.contentMode = .scaleAspectFit
        obj.translatesAutoresizingMaskIntoConstraints = false
        return obj
    }()
    
    lazy var formView: UIView = {
        let obj = UIView()
        obj.backgroundColor = .clear
        obj.translatesAutoresizingMaskIntoConstraints = false
        return obj
    }()
    
    lazy var usuarioTextField: UITextField = { [weak self] in
        guard let me = self else {return UITextField()}
        let obj = UITextField()
        obj.tag = 1
        obj.placeholder = "E-mail"
        obj.keyboardType = .emailAddress
        obj.autocorrectionType = .no
        obj.textAlignment = .center
        obj.layer.cornerRadius = me.constantes.loginStruct.textFieldCornerRadius
        obj.layer.borderColor = me.constantes.loginStruct.textFieldBorderColor
        obj.layer.borderWidth = me.constantes.loginStruct.textFieldBorderWidth
        obj.autocapitalizationType = .none
        obj.textColor = me.constantes.loginStruct.textFieldTextColor
        obj.text = Usuario.getUltimoEmailLogado()
        obj.translatesAutoresizingMaskIntoConstraints = false
        return obj
    }()
    
    lazy var senhaTextField: UITextField = { [weak self] in
        guard let me = self else {return UITextField()}
        let obj = UITextField()
        obj.tag = 2
        obj.placeholder = "Senha"
        obj.keyboardType = .default
        obj.autocorrectionType = .no
        obj.isSecureTextEntry = true
        obj.textAlignment = .center
        obj.layer.cornerRadius = me.constantes.loginStruct.textFieldCornerRadius
        obj.layer.borderColor = me.constantes.loginStruct.textFieldBorderColor
        obj.layer.borderWidth = me.constantes.loginStruct.textFieldBorderWidth
        obj.autocapitalizationType = .none
        obj.textColor = me.constantes.loginStruct.textFieldTextColor
        obj.translatesAutoresizingMaskIntoConstraints = false
        return obj
    }()
    
    lazy var esqueciSenhaBTN: UIButton = { [weak self] in
        let obj = UIButton()
        guard let me = self else { return obj }
        obj.setTitle("Esqueceu a senha? Clique aqui!", for: .normal)
        obj.setTitleColor(me.constantes.loginStruct.labelTextColor, for: .normal)
        obj.translatesAutoresizingMaskIntoConstraints = false
        obj.addTarget(me, action: #selector(me.esqueciSenhaAction), for: .touchUpInside)
        return obj
    }()
    
    lazy var okButton: UIButton = { [weak self] in
        let obj = UIButton()
        guard let me = self else { return obj }
        obj.setTitle("Entrar", for: .normal)
        obj.setTitleColor(me.constantes.loginStruct.buttonTextColor, for: .normal)
        obj.translatesAutoresizingMaskIntoConstraints = false
        obj.layer.cornerRadius = me.constantes.loginStruct.buttonCornerRadius
        obj.layer.borderWidth = me.constantes.loginStruct.buttonBorderWidth
        obj.layer.borderColor = me.constantes.loginStruct.buttonBorderColor
        obj.backgroundColor = me.constantes.loginStruct.buttonBackgroundColor
        if let me = self {
            obj.addTarget(me, action: #selector(me.okAction), for: .touchUpInside)
        }
        return obj
    }()
    
    lazy var cadastroButton: UIButton = { [weak self] in
        let obj = UIButton()
        guard let me = self else { return obj }
        obj.setTitle("Ou faça seu cadastro", for: .normal)
        obj.setTitleColor(me.constantes.loginStruct.labelTextColor, for: .normal)
        obj.translatesAutoresizingMaskIntoConstraints = false
        obj.addTarget(me, action: #selector(me.cadastroAction), for: .touchUpInside)
        return obj
    }()
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = self.constantes.loginStruct.backgroundColor
        self.viewModel.delegate = self
        self.viewModel.loginSilencioso()
        self.setupConstraints()
        self.registerForKeyboardNotification(initialValueForConstraint: -80)
    }
    
    func setupConstraints() {
        self.view.addSubview(self.imageViewLogo)
        self.view.addSubview(self.imageViewSubLogo)
                
        self.formView.addSubview(self.okButton)
        self.formView.addSubview(self.usuarioTextField)
        self.formView.addSubview(self.senhaTextField)
        
        self.view.addSubview(self.formView)
        
        self.view.addSubview(self.cadastroButton)
        self.view.addSubview(self.esqueciSenhaBTN)
                
        let topAnchor = self.imageViewLogo.topAnchor.constraint(equalTo: self.view.topAnchor, constant: self.view.bounds.height / 5)
        topAnchor.priority = .defaultLow
        topAnchor.isActive = true
        
        let constraint = self.imageViewLogo.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.15)
        constraint.priority = .defaultHigh
        constraint.isActive = true
        self.imageViewLogo.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 30).isActive = true
        self.imageViewLogo.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -30).isActive = true
        
        let imageTopConstraint = self.imageViewSubLogo.topAnchor.constraint(equalTo: self.imageViewLogo.bottomAnchor, constant: -3)
        imageTopConstraint.priority = .required
        imageTopConstraint.isActive = true
        
        self.imageViewSubLogo.centerXAnchor.constraint(equalTo: self.imageViewLogo.centerXAnchor).isActive = true
        
        self.imageViewSubLogo.heightAnchor.constraint(equalToConstant: 60).isActive = true
        self.imageViewSubLogo.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.5).isActive = true
        
        let bottom = self.imageViewSubLogo.bottomAnchor.constraint(lessThanOrEqualTo: self.formView.topAnchor)
        bottom.priority = .required
        bottom.isActive = true
        
        
        self.constraintButton = formView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -80)
        self.constraintButton?.isActive = true
        formView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        formView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.85).isActive = true
        formView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.4).isActive = true
        
        let widthView = self.view.frame.width * 0.85
        let widthFormComps: CGFloat = widthView < 320 ? widthView : 320
        let heightFormComps: CGFloat = 38
        
        self.okButton.bottomAnchor.constraint(equalTo: self.formView.bottomAnchor, constant: -10).isActive = true
        self.okButton.centerXAnchor.constraint(equalTo: self.formView.centerXAnchor).isActive = true
        self.okButton.widthAnchor.constraint(equalToConstant: widthFormComps).isActive = true
        self.okButton.heightAnchor.constraint(equalToConstant: heightFormComps).isActive = true
        
        self.cadastroButton.bottomAnchor.constraint(equalTo: self.okButton.topAnchor, constant: -5).isActive = true
        self.cadastroButton.centerXAnchor.constraint(equalTo: self.okButton.centerXAnchor).isActive = true
        
        self.esqueciSenhaBTN.bottomAnchor.constraint(equalTo: self.cadastroButton.topAnchor, constant: -5).isActive = true
        self.esqueciSenhaBTN.centerXAnchor.constraint(equalTo: self.cadastroButton.centerXAnchor).isActive = true
        
                
        self.senhaTextField.bottomAnchor.constraint(equalTo: self.esqueciSenhaBTN.topAnchor, constant: -5).isActive = true
        self.senhaTextField.centerXAnchor.constraint(equalTo: self.esqueciSenhaBTN.centerXAnchor).isActive = true
        self.senhaTextField.widthAnchor.constraint(equalToConstant: widthFormComps).isActive = true
        self.senhaTextField.heightAnchor.constraint(equalToConstant: heightFormComps).isActive = true
        
        self.usuarioTextField.bottomAnchor.constraint(equalTo: self.senhaTextField.topAnchor, constant: -22).isActive = true
        self.usuarioTextField.centerXAnchor.constraint(equalTo: self.senhaTextField.centerXAnchor).isActive = true
        self.usuarioTextField.widthAnchor.constraint(equalToConstant: widthFormComps).isActive = true
        self.usuarioTextField.heightAnchor.constraint(equalToConstant: heightFormComps).isActive = true
        
        self.view.updateConstraints()
    }
    
    
    
    @objc func okAction() {
        DispatchQueue.main.async { [weak self] in
            guard let me = self else { return }
            me.viewModel.login(usuario: me.usuarioTextField.text ?? "", senha: me.senhaTextField.text ?? "")
        }
        
    }
    
    @objc func esqueciSenhaAction() {
        ViewManager.esqueciSenhaViewController(vc: self)
    }
    
    @objc func cadastroAction() {
        ViewManager.cadastroViewController(vc: self)
    }
        
}

extension LoginViewController: LoginViewModelDelegate, CadastroViewControllerDelegate {
    func loginComNovoCadastro(email: String, senha: String) {
        self.viewModel.login(usuario: email, senha: senha)
    }
    
    func carregando() {
        self.showActivity()
    }
    
    func finalizarCarregamento() {
        self.hideActivity()
    }
    
    func finalizandoRequestComSucesso() {
        ViewManager.mainViewController(vc: self)
    }
    
    func finalizandoComErro(message: String) {
        ViewManager.alert(with: "Atenção", message: message, in: self, confirmActionHandler: nil, cancelAction: nil, completion: nil)
    }
    
    
}


