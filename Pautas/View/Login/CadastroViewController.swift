//
//  CadastroViewController.swift
//  Pautas
//
//  Created by José C Feitosa on 19/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import UIKit

protocol CadastroViewControllerDelegate: class {
    func loginComNovoCadastro(email: String, senha: String)
}

class CadastroViewController: DefaultViewController {
    
    var cellStrucs: [CadastroCellStruct] = Constantes().cadastroCellStructs
    
    weak var delegate: CadastroViewControllerDelegate?
    
    var nome = ""
    var email = ""
    var senha = ""
    
    
    lazy var tableView: UITableView = {
        let obj = UITableView()
        obj.translatesAutoresizingMaskIntoConstraints = false
        return obj
    }()
    
    lazy var topView: UIView = {
        let obj = UIView()
        obj.backgroundColor = .clear
        obj.translatesAutoresizingMaskIntoConstraints = false
        return obj
    }()
    
    lazy var titleLabelTop: UILabel = {
        let obj = UILabel()
        let constants = Constantes()
        obj.text = "Cadastre-se"
        obj.textColor = constants.loginStruct.labelTextColor
        obj.translatesAutoresizingMaskIntoConstraints = false
        return obj
    }()
    
    lazy var returnButton: UIButton = { [weak self] in
        let obj = UIButton()
        guard let me = self else { return obj }
        let constants = me.constantes
        obj.setImage(constants.voltarButtonImage, for: .normal)
        obj.imageView?.contentMode = .scaleAspectFit
        obj.tintColor = constants.loginStruct.labelTextColor
        obj.translatesAutoresizingMaskIntoConstraints = false
        obj.addTarget(me, action: #selector(me.closeMe), for: .touchUpInside)
        return obj
    }()
    
    
    lazy var descriptionLabelBottom: UILabel = {
        let obj = UILabel()
        let constants = Constantes()
        obj.font = UIFont(name: obj.font.fontName, size: 11)
        obj.text = "* Preencha todos os campos"
        obj.textColor = constants.loginStruct.labelTextColor
        obj.translatesAutoresizingMaskIntoConstraints = false
        return obj
    }()
    
    lazy var okButton: UIButton = { [weak self] in
        let obj = UIButton()
        guard let me = self else { return obj }
        let constants = me.constantes
        obj.setTitle("Cadastrar", for: .normal)
        obj.setTitleColor(constants.loginStruct.buttonTextColor, for: .normal)
        obj.backgroundColor = constants.loginStruct.buttonBackgroundColor
        obj.layer.cornerRadius = constants.loginStruct.buttonCornerRadius
        obj.layer.borderColor = constants.loginStruct.buttonBorderColor
        obj.layer.borderWidth = constants.loginStruct.buttonBorderWidth
        obj.translatesAutoresizingMaskIntoConstraints = false
        obj.addTarget(me, action: #selector(me.signup), for: .touchUpInside)
        return obj
    }()
    

    var viewModel = CadastroViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViewController()
        NotificationCenter.default.addObserver(self, selector: #selector(self.kbWillShow(note:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.kbWillShow(note:)), name: UIResponder.keyboardDidHideNotification, object: nil)
        self.view.backgroundColor = constantes.loginStruct.backgroundColor
        self.viewModel.delegate = self
    }
    
    @objc func kbWillShow(note: NSNotification) {
        if let newFrame = (note.userInfo?[ UIResponder.keyboardFrameEndUserInfoKey ] as? NSValue)?.cgRectValue {
            if (note.name.rawValue == UIResponder.keyboardDidShowNotification.rawValue) {
                let insets = UIEdgeInsets( top: 0, left: 0, bottom: newFrame.height, right: 0 )
                tableView.contentInset = insets
                tableView.scrollIndicatorInsets = insets
            } else {
                if let indexPath = tableView.indexPathForRow(at: .zero) {
                    tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                }
                
            }
            
        }
    }
    
    @objc func closeMe() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func signup() {
        self.viewModel.cadastrar(nome: nome, email: email, senha: senha)
    }
    
    func setupViewController() {
        self.view.addSubview(self.tableView)
        self.topView.addSubview(self.returnButton)
        self.topView.addSubview(self.titleLabelTop)
        
        let heightTopView: CGFloat = 63
       
        
        self.view.addSubview(self.topView)
        self.view.addSubview(self.okButton)
        self.view.addSubview(self.descriptionLabelBottom)
        
        
        self.topView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.topView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.topView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        self.topView.heightAnchor.constraint(equalToConstant: heightTopView).isActive = true
        
        self.titleLabelTop.topAnchor.constraint(equalTo: self.topView.topAnchor, constant: 35).isActive = true
        self.titleLabelTop.centerXAnchor.constraint(equalTo: self.topView.centerXAnchor).isActive = true
        
        self.returnButton.centerYAnchor.constraint(equalTo: self.titleLabelTop.centerYAnchor).isActive = true
        self.returnButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        self.returnButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.returnButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        self.okButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -50).isActive = true
        self.okButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.okButton.heightAnchor.constraint(equalToConstant: 35).isActive = true
        self.okButton.widthAnchor.constraint(equalToConstant: self.view.frame.width * 0.7).isActive = true
        
        self.descriptionLabelBottom.bottomAnchor.constraint(equalTo: self.okButton.topAnchor, constant: -10).isActive = true
        self.descriptionLabelBottom.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        
        self.tableView.register(CadastroTableViewCell.self, forCellReuseIdentifier: "SignupTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        

        
        self.tableView.topAnchor.constraint(equalTo: self.topView.bottomAnchor, constant: 6).isActive = true
        
        self.tableView.bottomAnchor.constraint(equalTo: self.descriptionLabelBottom.topAnchor).isActive = true
        self.tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        self.tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
        self.tableView.separatorStyle = .none
        self.tableView.reloadData()
        self.tableView.backgroundColor = .clear
    }
    
}

extension CadastroViewController: CadastroViewModelDelegate, CadastroCellDelegate {
    func mudando(textField: UITextField) {
        switch (textField.placeholder ?? "").lowercased() {
        case "nome":
            self.nome = textField.text ?? ""
            break
        case "e-mail":
            self.email = textField.text ?? ""
            break
        case "senha":
            self.senha = textField.text ?? ""
            break
        default:
            print("nenhum dos cases")
        }
    }
    
    func carregando() {
        self.showActivity()
    }
    
    func finalizarCarregamento() {
        self.hideActivity()
    }
    
    func finalizandoComSucesso() {
        ViewManager.alert(with: "Cadastro", message: "Cadastro realizado com sucesso!", in: self, confirmActionHandler: { [weak self] (_) in
            DispatchQueue.main.async {
                self?.delegate?.loginComNovoCadastro(email: self?.email ?? "", senha: self?.senha ?? "")
                self?.dismiss(animated: true, completion: nil)
            }
        }, cancelAction: nil, completion: nil)
    }
    
    func finalizandoComErro(message: String) {
        ViewManager.alert(with: "Atenção", message: message, in: self, confirmActionHandler: nil, cancelAction: nil, completion: nil)
    }
}

extension CadastroViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cellStrucs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SignupTableViewCell") as? CadastroTableViewCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        cell.configure(cellStruct: self.cellStrucs[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.setSelected(false, animated: true)
        cell?.endEditing(false)
    }
    
}

protocol CadastroCellDelegate: class {
    func mudando(textField: UITextField)
}

class CadastroTableViewCell: UITableViewCell {
    
    private var cellStruct: CadastroCellStruct?
    
    weak var delegate: CadastroCellDelegate?
    
    lazy var textField: UITextField = { [weak self] in
        guard let me = self else {return UITextField()}
        let constants = Constantes()
        guard let myStruct = me.cellStruct else {return UITextField()}
        let obj = UITextField()
        obj.placeholder = myStruct.placeholderTextField
        obj.keyboardType = myStruct.keyboradTypeTextField
        obj.autocorrectionType = .no
        obj.textAlignment = .center
        obj.layer.cornerRadius = myStruct.cornerRadiusTextField
        obj.layer.borderColor = myStruct.borderColorTextField
        obj.layer.borderWidth = myStruct.borderWidthTextField
        obj.autocapitalizationType = myStruct.autoCapitalizeTextField
        obj.textColor = myStruct.textColorTextField
        obj.isSecureTextEntry = myStruct.isSecuryEntry
        obj.translatesAutoresizingMaskIntoConstraints = false
        obj.delegate = me
        me.contentView.addSubview(obj)
        return obj
    }()
    
    func configure(cellStruct: CadastroCellStruct) {
        self.cellStruct = cellStruct
        self.setupCell()
    }
    
    private func setupCell() {
        self.contentView.backgroundColor = .clear
        self.backgroundColor = .clear
        
        
        
        self.textField.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 15).isActive = true
        self.textField.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -15).isActive = true
        self.textField.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor).isActive = true
        self.textField.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor).isActive = true
        
        
    }
}

extension CadastroTableViewCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.delegate?.mudando(textField: textField)
        return true
    }
}

