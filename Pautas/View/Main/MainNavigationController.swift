//
//  MainNavigationController.swift
//  Pautas
//
//  Created by José C Feitosa on 19/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {
    
    let constantes = Constantes()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            self.overrideUserInterfaceStyle = .light
        } else {
        }
        self.setup()
    }
    
    func setup() {
        self.navigationBar.barTintColor = constantes.mainViewStruct.navBarBackgroundColor
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: constantes.mainViewStruct.navBarTextColor]
    }

}
