//
//  MainViewController.swift
//  Pautas
//
//  Created by José C Feitosa on 20/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import UIKit

class MainViewController: DefaultViewController {
    
    lazy var searchBar: UISearchBar = { [weak self] in
        let obj = UISearchBar()
        obj.translatesAutoresizingMaskIntoConstraints = false
        guard let me = self else { return obj }
        obj.backgroundImage = UIImage()
        let colorText = me.constantes.mainViewStruct.navBarTextColor
        let colorBack = me.constantes.mainViewStruct.navBarBackgroundColor
        obj.setTextColor(color: colorText)
        obj.setSearchImageColor(color: colorBack)
        
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: me.view.bounds.width, height: 35))
        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: me, action: nil)
        let button = UIBarButtonItem(title: "Ok", style: UIBarButtonItem.Style.plain, target: me, action: #selector(me.touched))
        toolBar.items = [space, button]
        obj.inputAccessoryView = toolBar
        
        
        obj.scopeButtonTitles = ["Abertas","Finalizadas"]
        obj.selectedScopeButtonIndex = 0
        
        
        
        guard let textFieldInsideSearchBar = obj.getSearchBarTextField() else{
            return obj
        }
        let pointSize = textFieldInsideSearchBar.font!.pointSize
        textFieldInsideSearchBar.layer.cornerRadius = 10.0
        textFieldInsideSearchBar.attributedPlaceholder =  NSAttributedString(string: "Buscar", attributes: [.foregroundColor: colorText])

        return obj
    }()
    
    lazy var tableView: UITableView = {
        let tbv = UITableView()
        tbv.translatesAutoresizingMaskIntoConstraints = false
        tbv.separatorStyle = .none
        tbv.backgroundColor = .clear
        return tbv
    }()
    
    lazy var label: UILabel = {
        let obj = UILabel()
        obj.translatesAutoresizingMaskIntoConstraints = false
        obj.textColor = .darkGray
        obj.text = "Sem dados"
        return obj
    }()
    
    var cellStructs = [MainCellStruct]() {
        didSet {
            self.label.isHidden = self.cellStructs.count > 0
            self.tableView.reloadData()
        }
    }

    var viewModel = MainViewModel()
    
    var selectedIndex = -1 {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigation("Pautas")
        self.setupNavButtons()
        self.view.backgroundColor = self.constantes.mainViewStruct.viewBackgroundColor
        self.setupView()
        
        self.searchBar.delegate = self
        
        self.tableView.register(PautaTableViewCell.self, forCellReuseIdentifier: "cell")
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.viewModel.delegate = self
        self.viewModel.buscar(valor: "", abertas: true)
    }
    
    @objc func touched() {
        self.view.endEditing(true)
    }
    
    func setupView() {
        self.view.addSubview(self.searchBar)
        self.searchBar.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        self.searchBar.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.searchBar.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        self.view.addSubview(self.tableView)
        self.tableView.topAnchor.constraint(equalTo: self.searchBar.bottomAnchor).isActive = true
        self.tableView.leadingAnchor.constraint(equalTo: self.searchBar.leadingAnchor).isActive = true
        self.tableView.trailingAnchor.constraint(equalTo: self.searchBar.trailingAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        self.view.addSubview(self.label)
        self.label.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.label.centerYAnchor.constraint(equalToSystemSpacingBelow: self.view.centerYAnchor, multiplier: 0.8).isActive = true
        
        self.view.layoutIfNeeded()
    }
    
    func setupNavButtons() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: constantes.usuarioButtonImage, style: .plain, target: self, action: #selector(self.showUsuarioDetalhes))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: constantes.addButtonImage, style: .plain, target: self, action: #selector(self.addPauta))
    }
    
    @objc func showUsuarioDetalhes() {
        ViewManager.showAlertOptionsUser(in: self, firstActionHandler: { (_) in
            //Mostrar Detalhes do usuário
            ViewManager.alert(with: "Detalhes do usuário", message: "\(Usuario.shared?.nome ?? "-")\n\(Usuario.shared?.email ?? "-")", in: self, confirmActionHandler: nil, cancelAction: nil, completion: nil)
        }, secondActionHandler: { (_) in
            //Logout
            Usuario.shared?.logout()
            self.dismiss(animated: true, completion: nil)
        }, completion: nil)
    }
    
    @objc func addPauta() {
        ViewManager.cadastroEditViewController(vc: self)
    }
    
    
}

extension MainViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.viewModel.buscar(valor: "", abertas: true)
        searchBar.text = ""
        searchBar.selectedScopeButtonIndex = 0
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsScopeBar = false
        searchBar.showsCancelButton = false
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.viewModel.buscar(valor: searchBar.text ?? "", abertas: searchBar.selectedScopeButtonIndex == 0)
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsScopeBar = true
        searchBar.showsCancelButton = true
        
    }
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        self.selectedIndex = -1
        self.viewModel.buscar(valor: searchBar.text ?? "", abertas: selectedScope == 0)
    }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cellStructs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? PautaTableViewCell {
            cell.setupCell(self.cellStructs[indexPath.row].data)
            cell.delegate = self
            cell.index = indexPath.row
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.selectedIndex == indexPath.row ? 400 : 120
    }
    
    
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}


extension MainViewController: MainViewModelDelegate, PautaCellDelegate, CadastroEditViewControllerDelegate {
    func reloadTableView() {
        searchBar.text = ""
        searchBar.selectedScopeButtonIndex = 0
        self.viewModel.buscar(valor: "", abertas: true)
    }
    
    func dataToggle(index: Int) {
        self.selectedIndex = -1
        let pauta = self.cellStructs[index].data
        pauta.toggle { [weak self] in
            self?.viewModel.buscar(valor: self?.searchBar.text ?? "", abertas: self?.searchBar.selectedScopeButtonIndex == 0)
           
        }
    }
    
    func carregando() {
        self.showActivity()
    }
    
    func finalizandoCarregamento() {
        self.hideActivity()
    }
    
    func finalizandoComSucesso(cellStruct: [MainCellStruct]) {
        self.cellStructs = cellStruct
    }
    
    func didSlectCell(index: Int) {
        DispatchQueue.main.async {
            self.selectedIndex = self.selectedIndex == index ? -1 : index
        }
        
    }
}
