//
//  CadastroPautaViewController.swift
//  Pautas
//
//  Created by José C Feitosa on 19/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import UIKit

protocol CadastroEditViewControllerDelegate: class {
    func reloadTableView()
}

class CadastroEditPautaViewController: DefaultViewController {
    var cellStrucs: [CadastroCellStruct] = Constantes().cadastroEditCellStructs
    
    weak var delegate: CadastroEditViewControllerDelegate?
    
    var titulo = ""
    var descricao = ""
    var detalhes = ""
    
    var pauta: Pauta?
    
    
    lazy var tableView: UITableView = {
        let obj = UITableView()
        obj.translatesAutoresizingMaskIntoConstraints = false
        return obj
    }()
    
    lazy var topView: UIView = {
        let obj = UIView()
        obj.backgroundColor = .clear
        obj.translatesAutoresizingMaskIntoConstraints = false
        return obj
    }()
    
    lazy var titleLabelTop: UILabel = { [weak self] in
        let obj = UILabel()
        guard let me = self else {
            return obj
        }
        let constants = Constantes()
        // para futuras implementacoes de edicao
        obj.text = me.pauta == nil ? "Cadastre uma pauta!" : "Edite a pauta"
        obj.textColor = constants.loginStruct.labelTextColor
        obj.translatesAutoresizingMaskIntoConstraints = false
        return obj
    }()
    
    lazy var returnButton: UIButton = { [weak self] in
        let obj = UIButton()
        guard let me = self else { return obj }
        let constants = me.constantes
        obj.setImage(constants.voltarButtonImage, for: .normal)
        obj.imageView?.contentMode = .scaleAspectFit
        obj.tintColor = constants.loginStruct.labelTextColor
        obj.translatesAutoresizingMaskIntoConstraints = false
        obj.addTarget(me, action: #selector(me.closeMe), for: .touchUpInside)
        return obj
        }()
    
    
    lazy var descriptionLabelBottom: UILabel = {
        let obj = UILabel()
        let constants = Constantes()
        obj.font = UIFont(name: obj.font.fontName, size: 11)
        obj.text = "* Preencha todos os campos"
        obj.textColor = constants.loginStruct.labelTextColor
        obj.translatesAutoresizingMaskIntoConstraints = false
        return obj
    }()
    
    lazy var okButton: UIButton = { [weak self] in
        let obj = UIButton()
        guard let me = self else { return obj }
        let constants = me.constantes
        let title = me.pauta == nil ? "Cadastrar" : "Atualizar"
        obj.setTitle(title, for: .normal)
        obj.setTitleColor(constants.loginStruct.buttonTextColor, for: .normal)
        obj.backgroundColor = constants.loginStruct.buttonBackgroundColor
        obj.layer.cornerRadius = constants.loginStruct.buttonCornerRadius
        obj.layer.borderColor = constants.loginStruct.buttonBorderColor
        obj.layer.borderWidth = constants.loginStruct.buttonBorderWidth
        obj.translatesAutoresizingMaskIntoConstraints = false
        obj.addTarget(me, action: #selector(me.save), for: .touchUpInside)
        return obj
        }()
    
    
    var viewModel = CadastroEditPautaViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViewController()
        NotificationCenter.default.addObserver(self, selector: #selector(self.kbWillShow(note:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.kbWillShow(note:)), name: UIResponder.keyboardDidHideNotification, object: nil)
        self.view.backgroundColor = constantes.loginStruct.backgroundColor
        self.viewModel.delegate = self
    }
    
    @objc func kbWillShow(note: NSNotification) {
        if let newFrame = (note.userInfo?[ UIResponder.keyboardFrameEndUserInfoKey ] as? NSValue)?.cgRectValue {
            if (note.name.rawValue == UIResponder.keyboardDidShowNotification.rawValue) {
                let insets = UIEdgeInsets( top: 0, left: 0, bottom: newFrame.height, right: 0 )
                tableView.contentInset = insets
                tableView.scrollIndicatorInsets = insets
            } else {
                if let indexPath = tableView.indexPathForRow(at: .zero) {
                    tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                }
                
            }
            
        }
    }
    
    @objc func closeMe() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func save() {
        
        self.viewModel.salvar(titulo: self.titulo, descricao: self.descricao, detalhes: self.detalhes, pauta: self.pauta)
    }
    
    func setupViewController() {
        self.view.addSubview(self.tableView)
        self.topView.addSubview(self.returnButton)
        self.topView.addSubview(self.titleLabelTop)
        
        let heightTopView: CGFloat = 63
        
        
        self.view.addSubview(self.topView)
        self.view.addSubview(self.okButton)
        self.view.addSubview(self.descriptionLabelBottom)
        
        
        self.topView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.topView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.topView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        self.topView.heightAnchor.constraint(equalToConstant: heightTopView).isActive = true
        
        self.titleLabelTop.topAnchor.constraint(equalTo: self.topView.topAnchor, constant: 35).isActive = true
        self.titleLabelTop.centerXAnchor.constraint(equalTo: self.topView.centerXAnchor).isActive = true
        
        self.returnButton.centerYAnchor.constraint(equalTo: self.titleLabelTop.centerYAnchor).isActive = true
        self.returnButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        self.returnButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.returnButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        self.okButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -50).isActive = true
        self.okButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.okButton.heightAnchor.constraint(equalToConstant: 35).isActive = true
        self.okButton.widthAnchor.constraint(equalToConstant: self.view.frame.width * 0.7).isActive = true
        
        self.descriptionLabelBottom.bottomAnchor.constraint(equalTo: self.okButton.topAnchor, constant: -10).isActive = true
        self.descriptionLabelBottom.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        
        self.tableView.register(CadastroTableViewCell.self, forCellReuseIdentifier: "CadastroTableViewCell")
        self.tableView.register(CadastroTextViewTableViewCell.self, forCellReuseIdentifier: "CadastroTextViewTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        
        
        self.tableView.topAnchor.constraint(equalTo: self.topView.bottomAnchor, constant: 6).isActive = true
        
        self.tableView.bottomAnchor.constraint(equalTo: self.descriptionLabelBottom.topAnchor).isActive = true
        self.tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        self.tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
        self.tableView.separatorStyle = .none
        self.tableView.reloadData()
        self.tableView.backgroundColor = .clear
    }
    
}

extension CadastroEditPautaViewController: CadastroEditPautaViewModelDelegate, CadastroCellDelegate, CadastroTextViewCellDelegate {
    func finalizandoComErro(message: String) {
        ViewManager.alert(with: "Atenção", message: message, in: self, confirmActionHandler: nil, cancelAction: nil, completion: nil)
    }
    
    func mudando(textField: UITextView) {
        self.detalhes = textField.text ?? ""
    }
    
    func finalizandoCarregamento() {
        self.hideActivity()
    }
    
    func finalizandoSalvamento() {
        self.delegate?.reloadTableView()
        self.dismiss(animated: true, completion: nil)
    }
     
    func mudando(textField: UITextField) {
        switch (textField.placeholder ?? "").lowercased() {
        case "título":
            self.titulo = textField.text ?? ""
            break
        case "descrição breve":
            self.descricao = textField.text ?? ""
            break
        
        default:
            print("nenhum dos cases")
        }
    }
    
    func carregando() {
        self.showActivity()
    }
    
    func finalizarCarregamento() {
        self.hideActivity()
    }
    
    
}

extension CadastroEditPautaViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cellStrucs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 2 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CadastroTextViewTableViewCell") as? CadastroTextViewTableViewCell else {
                return UITableViewCell()
            }
            cell.delegate = self
            cell.configure(cellStruct: self.cellStrucs[indexPath.row])
            return cell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CadastroTableViewCell") as? CadastroTableViewCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        cell.configure(cellStruct: self.cellStrucs[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 2 ? 200 : 70.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.setSelected(false, animated: true)
        cell?.endEditing(false)
    }
    
}

protocol CadastroTextViewCellDelegate: class {
    func mudando(textField: UITextView)
}


class CadastroTextViewTableViewCell: UITableViewCell {
    
    private var cellStruct: CadastroCellStruct?
    
    weak var delegate: CadastroTextViewCellDelegate?
    
    lazy var label: UILabel = { [weak self] in
        let obj = UILabel()
        guard let me = self else {
            return obj
        }
        obj.textColor = .groupTableViewBackground
        obj.translatesAutoresizingMaskIntoConstraints = false
        me.contentView.addSubview(obj)
        return obj
    }()
    
    lazy var textView: UITextView = { [weak self] in
        guard let me = self else {return UITextView()}
        let constants = Constantes()
        guard let myStruct = me.cellStruct else {return UITextView()}
        let obj = UITextView()
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: me.contentView.frame.width, height: 35))
        let separator = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: me, action: nil)
        let button = UIBarButtonItem(title: "Ok", style: .plain, target: me, action: #selector(me.touched))
        toolBar.items = [separator, button]
        obj.inputAccessoryView = toolBar
        obj.keyboardType = myStruct.keyboradTypeTextField
        obj.autocorrectionType = .no
        obj.textAlignment = .center
        obj.layer.cornerRadius = myStruct.cornerRadiusTextField
        obj.layer.borderColor = myStruct.borderColorTextField
        obj.layer.borderWidth = myStruct.borderWidthTextField
        obj.autocapitalizationType = myStruct.autoCapitalizeTextField
        obj.textColor = Constantes().mainColor
        obj.font = UIFont(name: obj.font?.fontName ?? "Arial", size: 18)
        obj.translatesAutoresizingMaskIntoConstraints = false
        obj.delegate = me
        //Para futura reutilização
        //obj.tag = myStruct.tag
        me.contentView.addSubview(obj)
        return obj
    }()
    
    func configure(cellStruct: CadastroCellStruct) {
        self.cellStruct = cellStruct
        self.label.text = cellStruct.placeholderTextField
        self.setupCell()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
    }
    
    private func setupCell() {
        self.contentView.backgroundColor = .clear
        self.backgroundColor = .clear
        
        self.label.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 5).isActive = true
        self.label.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor).isActive = true
        self.label.heightAnchor.constraint(equalToConstant: 18).isActive = true
        
        self.textView.topAnchor.constraint(equalTo: self.label.bottomAnchor, constant: 5).isActive = true
        self.textView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -15).isActive = true
        self.textView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor).isActive = true
        self.textView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor).isActive = true
        
        
    }
    
    @objc func touched() {
        self.endEditing(true)
    }
}

extension CadastroTextViewTableViewCell: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        self.delegate?.mudando(textField: textView)
    }
}

