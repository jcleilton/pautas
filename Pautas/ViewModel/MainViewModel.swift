//
//  MainViewModel.swift
//  Pautas
//
//  Created by José C Feitosa on 20/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import Foundation

protocol MainViewModelDelegate: class {
    func carregando()
    func finalizandoCarregamento()
    func finalizandoComSucesso(cellStruct: [MainCellStruct])
}

class MainViewModel {
    weak var delegate: MainViewModelDelegate?
    
    func buscar(
        valor: String,
        abertas: Bool
    ) {
        self.delegate?.carregando()
        Pauta.getPautas(abertas: abertas) { [weak self, valor] (pautas) in
            guard let me = self else {
                
                self?.delegate?.finalizandoCarregamento()
                return
                
            }
            let pautasFiltradas = me.filtrar(pautas: pautas, valor: valor)
            let cellStructs = me.mapear(pautas: pautasFiltradas)
            DispatchQueue.main.async {
                me.delegate?.finalizandoCarregamento()
                me.delegate?.finalizandoComSucesso(cellStruct: cellStructs)
            }
            
        }
    }
    
    private func mapear(pautas: [Pauta]) -> [MainCellStruct] {
        let cellStructs = pautas.compactMap{self.mapear(pauta: $0)}
        return cellStructs
    }
    
    
    private func filtrar(pauta: Pauta, valor: String) -> Bool {
        
        let user = Usuario.shared?.usuarioData
        let isMyOwn = (pauta.data?.autor?.id ?? -1) == (user?.id ?? -2)
        if valor == "" {
            return isMyOwn
        }
        return isMyOwn && (
            (pauta.titulo ?? "").lowercased().contains(valor.lowercased()) ||
            (pauta.descricao ?? "").lowercased().contains(valor.lowercased()) ||
            (pauta.detalhes ?? "").lowercased().contains(valor.lowercased())
        )
    }
    
    private func filtrar(pautas: [Pauta], valor: String) -> [Pauta] {
        let pautasFiltradas = pautas.filter{self.filtrar(pauta: $0, valor: valor)}
        return pautasFiltradas
    }
    
    private func mapear(pauta: Pauta) -> MainCellStruct {
        return MainCellStruct(expandida: false, data: pauta)
    }
}

