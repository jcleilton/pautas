//
//  LoginViewModel.swift
//  Pautas
//
//  Created by José C Feitosa on 18/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import Foundation

protocol LoginViewModelDelegate: class {
    func carregando()
    func finalizarCarregamento()
    func finalizandoRequestComSucesso()
    func finalizandoComErro(message: String)
}

class LoginViewModel {
    weak var delegate: LoginViewModelDelegate?
    
    func loginSilencioso() {
        self.delegate?.carregando()
        Usuario.recuperarUltimoUsuario { [weak self] (sucesso) in
            DispatchQueue.main.async {
                self?.delegate?.finalizarCarregamento()
            }
            if sucesso {
                DispatchQueue.main.async {
                    self?.delegate?.finalizandoRequestComSucesso()
                }
            }
        }
    }
    
    func login(usuario: String, senha: String) {
        let validador = Validador.credential(usuario: usuario, senha: senha)
        let status = validador["status"] as? Bool ?? false
        if !status {
            let message = validador["message"] as? String ?? ""
            self.delegate?.finalizandoComErro(message: message)
            return
        }
        let args = [
            "email" : usuario,
            "password" : senha
        ]
        let baseUrl = Constantes().rootUrl
        self.delegate?.carregando()
        ConnectionSessionManager.invoke(url: baseUrl + "/login", withArgs: args, httpMethod: .post) { [weak self] (data, error) in
            DispatchQueue.main.async {
                DispatchQueue.main.async {
                    self?.delegate?.finalizarCarregamento()
                }
                
            }
            
            guard let _ = error else {
                guard let data = data as? [String:Any] else {
                    self?.delegate?.finalizandoComErro(message: "Erro ao receber os dados")
                    return
                }
                let token = data["token"] as? String ?? ""
                let usuarioDict = data["usuario"] as? [String:Any] ?? [:]
                let user = Usuario(dict: usuarioDict, token: token)
                user.create()
                DispatchQueue.main.async {
                    user.salvarCredenciasParaLoginSilencioso()
                    self?.delegate?.finalizandoRequestComSucesso()
                }
                
                return
            }
            DispatchQueue.main.async {
                
                self?.delegate?.finalizandoComErro(message: "Verifique se email e senha estão corretos ou se sua conexão com a internet está ativa." )
            }
            
        }
    }
}
