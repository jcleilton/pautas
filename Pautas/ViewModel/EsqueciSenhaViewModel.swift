//
//  EsqueciSenhaViewModel.swift
//  Pautas
//
//  Created by TempDev on 20/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import Foundation


protocol EsqueciSenhaDelegate: class {
    func finalizandoComErro(message: String)
    func finalizandoComSucesso()
    func carregando()
    func finalizarCarregando()
}

class EsqueciSenhaViewModel {
    weak var delegate: EsqueciSenhaDelegate?
    
    func esqueciSenhaRequest(email: String) {
        let validarEmail = Validador.validarEmail(email: email)
        let status = validarEmail["status"] as? Bool ?? false
        if !status {
            let message = validarEmail["message"] as? String ?? ""
            self.delegate?.finalizandoComErro(message: message)
            return
        }
        let baseUrl = Constantes().rootUrl
        self.delegate?.carregando()
        ConnectionSessionManager.invokeWithoutData(url: baseUrl + "/recuperarsenha/\(email)", withArgs: [:], httpMethod: .get) { [weak self] (err) in
            DispatchQueue.main.async {
                self?.delegate?.finalizarCarregando()
            }
            guard let _ = err else {
                self?.delegate?.finalizandoComSucesso()
                return
            }
            DispatchQueue.main.async {
                self?.delegate?.finalizandoComErro(message: "Algo deu errado! Provavelmente o e-mail não está cadastrado.")
            }
        }
    }
}
