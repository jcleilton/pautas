//
//  CadastroEditPautaViewModel.swift
//  Pautas
//
//  Created by José C Feitosa on 20/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import Foundation

protocol CadastroEditPautaViewModelDelegate: class {
    func carregando()
    func finalizandoCarregamento()
    func finalizandoSalvamento()
    func finalizandoComErro(message: String)
}

class CadastroEditPautaViewModel {
    weak var delegate: CadastroEditPautaViewModelDelegate?
    
    func buscar() {
        
    }
    
    func salvar(
        titulo: String,
        descricao: String,
        detalhes: String,
        pauta: Pauta?
    ) {
        let validador = Validador.validarPauta(titulo: titulo, descricao: descricao, detalhes: detalhes)
        let status = validador["status"] as? Bool ?? false
        if !status {
            let message = validador["message"] as? String ?? ""
            self.delegate?.finalizandoComErro(message: message)
        }
        guard let pauta = pauta else {
            getContext { [weak self] (context) in
                if let context = context {
                    DispatchQueue.main.async {
                        let pauta = PautaData(context: context)
                        pauta.titulo = titulo
                        pauta.descricao = descricao
                        pauta.detalhes = detalhes
                        pauta.aberta = true
                        pauta.autor = Usuario.shared?.usuarioData
                        saveContext()
                        self?.delegate?.finalizandoSalvamento()
                    }
                    
                }
            }
            return
        }
        DispatchQueue.main.async { [weak self] in
            pauta.data?.titulo = titulo
            pauta.data?.descricao = descricao
            pauta.data?.detalhes = detalhes
            pauta.aberta = true
            pauta.data?.autor = Usuario.shared?.usuarioData
            saveContext()
            self?.delegate?.finalizandoSalvamento()
        }
    }
}
