//
//  CadastroViewModel.swift
//  Pautas
//
//  Created by TempDev on 20/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import Foundation

protocol CadastroViewModelDelegate: class {
    func carregando()
    func finalizarCarregamento()
    func finalizandoComSucesso()
    func finalizandoComErro(message: String)
}

class CadastroViewModel {
    weak var delegate: CadastroViewModelDelegate?
    
    func cadastrar(nome: String, email: String, senha: String) {
        let validador = Validador.validarCadastro(nome: nome, email: email, senha: senha)
        let status = validador["status"] as? Bool ?? false
        if !status {
            let message = validador["message"] as? String ?? ""
            self.delegate?.finalizandoComErro(message: message)
            return
        }
        let args = [
            "nome" : nome,
            "email" : email,
            "senha" : senha
        ]
        let baseUrl = Constantes().rootUrl
        self.delegate?.carregando()
        ConnectionSessionManager.invokeWithoutData(url: baseUrl + "/cadastro", withArgs: args, httpMethod: .post) { [weak self] (err) in
            DispatchQueue.main.async {
                self?.delegate?.finalizarCarregamento()
            }
            guard let _ = err else {
                DispatchQueue.main.async {
                    self?.delegate?.finalizandoComSucesso()
                }
                return
            }
            DispatchQueue.main.async {
                self?.delegate?.finalizandoComErro(message: "Não foi possível realizar o cadastro. Verifique os dados e tente novamente.")
            }
        }
    }
}
