//
//  Pautas.swift
//  Pautas
//
//  Created by José C Feitosa on 18/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import Foundation
import CoreData



class Pauta {
    var titulo: String?
    var descricao: String?
    var detalhes: String?
    var aberta: Bool = true
    var posicao: Int32 = 0
    
    var data: PautaData?
    
    init(data: PautaData?) {
        self.titulo = data?.titulo
        self.descricao = data?.descricao
        self.detalhes = data?.detalhes
        self.aberta = data?.aberta ?? false
        self.posicao = data?.posicao ?? 0
        
        self.data = data
    }
    
    public class func userFetchRequest() -> NSFetchRequest<PautaData> {
        return NSFetchRequest<PautaData>(entityName: "PautaData")
    }
    
    func saveData(completion: @escaping() -> Void) {
        if let data = self.data {
            data.titulo = self.titulo
            data.descricao = self.descricao
            data.detalhes = self.detalhes
            data.aberta = self.aberta
            data.posicao = Constantes().getLastPosition()
            DispatchQueue.main.async {
                saveContext()
                completion()
            }
        } else {
            getContext { [weak self] (context) in
                if let context = context {
                    let data = PautaData(context: context)
                    data.titulo = self?.titulo
                    data.descricao = self?.descricao
                    data.detalhes = self?.detalhes
                    data.aberta = self?.aberta ?? false
                    data.posicao = Constantes().getLastPosition()
                    DispatchQueue.main.async {
                        saveContext()
                        completion()
                    }
                }
            }
        }
    }
    
    func toggle(completion: @escaping() -> Void) {
        self.aberta = !self.aberta
        saveData(completion: completion)
    }
    
    
    class func getPautas(abertas: Bool, completion: @escaping([Pauta]) -> Void) {
        let request = userFetchRequest()
        request.predicate = NSPredicate(format: "aberta == %@", NSNumber(value: abertas))
        request.returnsObjectsAsFaults = false
        getContext { [request, completion] (context) in
            if let context = context {
                do {
                    let result = try context.fetch(request)
                    let pautas = (result).compactMap{Pauta(data: $0)}
                    return completion(pautas)
                } catch {
                    print(error.localizedDescription)
                    return completion([])
                }
            } else {
                return completion([])
            }
        }
    }
}
