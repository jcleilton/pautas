//
//  Global.swift
//  Pautas
//
//  Created by José C Feitosa on 18/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import UIKit
import CoreData

func getContext(completion: @escaping(NSManagedObjectContext?) -> Void) {
    DispatchQueue.main.async {
        if let app = UIApplication.shared.delegate as? AppDelegate {
            let context = app.persistentContainer.viewContext
            completion(context)
        }
    }
}

func saveContext() {
    getContext { (context) in
        if let context = context {
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
