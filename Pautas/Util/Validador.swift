//
//  Validador.swift
//  Pautas
//
//  Created by José C Feitosa on 19/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import Foundation

class Validador {
    class func credential(usuario: String, senha: String) -> [String:Any] {
        
        let validarEmail = Validador.validarEmail(email: usuario)
        let status = validarEmail["status"] as? Bool ?? false
        if !status {
            let message = validarEmail["message"] as? String ?? ""
            return [
                "status" : false,
                "message" : message
            ]
        }
        
        if senha.isEmpty {
            return [
                "status" : false,
                "message" : "O campo senha é requerido."
            ]
        }
        
        if senha.count < 6 {
            return [
                "status" : false,
                "message" : "Senha precisa ter pelo menos 6 caracteres."
            ]
        }
        return [
            "status" : true,
            "message" : ""
        ]
    }
    
    class func validarEmail(email: String) -> [String:Any] {
        if email.isEmpty {
            return [
                "status" : false,
                "message" : "O campo email é requerido."
            ]
        }
        
        if !email.isValidEmail() {
            return [
                "status" : false,
                "message" : "Digite um email válido"
            ]
        }
        
        return [
            "status" : true,
            "message" : ""
        ]
    }
    
    class func validarCadastro(nome: String, email: String, senha: String) -> [String:Any] {
        let validarEmail = Validador.credential(usuario: email, senha: senha)
        let status = validarEmail["status"] as? Bool ?? false
        if !status {
            let message = validarEmail["message"] as? String ?? ""
            return [
                "status" : false,
                "message" : message
            ]
        }
        if nome.count < 2 {
            return [
                "status" : false,
                "message" : "Verifique o campo nome e tente novamente."
            ]
        }
        return [
            "status" : true,
            "message" : ""
        ]
    }
    
    class func validarPauta(titulo: String, descricao: String, detalhes: String) -> [String:Any] {
        
        if
                (titulo.count < 2) ||
                (descricao.count < 2) ||
                (detalhes.count < 2)
        
        {
            return [
                "status" : false,
                "message" : "Verifique se todos os campos estão preenchidos."
            ]
        }
        return [
            "status" : true,
            "message" : ""
        ]
    }
}
