//
//  Constantes.swift
//  Pautas
//
//  Created by José C Feitosa on 18/01/20.
//  Copyright © 2020 José C Feitosa. All rights reserved.
//

import UIKit

class Constantes {
    let flavor: FLAVORS = {
        #if DEV
        return FLAVORS.dev
        #else
        return FLAVORS.prod
        #endif
    }()
    
    //Criei um serviço em node js para login e recuperação de senha. Devido ao pouco tempo, não fiz o CRUD completo da aplicação.
    lazy var rootUrl: String = { [weak self] in
        guard let me = self else { return "http://192.168.0.101:3000" }
        if (me.flavor == .dev) {
            return "http://192.168.0.101:3000"
        }
        return "https://pure-fortress-43101.herokuapp.com"
    }()
    
    //Para identificar o ambiente de teste, coloquei uma cor diferente para o app.
    lazy var mainColor: UIColor = { [weak self] in
        guard let me = self else { return .white }
        return (me.flavor == .prod) ? UIColor(red: 15/255, green: 140/255, blue: 250/255, alpha: 1) : UIColor.purple
    }()
    
    lazy var loginStruct: LoginViewStruct = { [weak self] in
        let producao = self?.flavor == .prod
        return LoginViewStruct(
            imagemLogo: producao ? UIImage(named: "logo-branco") ?? UIImage() : UIImage(),
            imagemSublogo: producao ? UIImage(named: "logo-completa-preto") ?? UIImage() : UIImage(),
            backgroundColor: self?.mainColor ?? .blue,
            textFieldTextColor: .white,
            textFieldBorderWidth: 0.4,
            textFieldCornerRadius: 12,
            textFieldBorderColor: UIColor.white.cgColor,
            buttonTextColor: self?.mainColor ?? .blue,
            buttonBackgroundColor: .white,
            buttonCornerRadius: 12,
            buttonBorderWidth: 0.4,
            buttonBorderColor: UIColor.black.cgColor,
            labelTextColor: .white
        )
    }()
    
    let voltarButtonImage = (UIImage(named: "icon_close") ?? UIImage()).withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
    let usuarioButtonImage = (UIImage(named: "icon_user") ?? UIImage()).withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
    let addButtonImage = (UIImage(named: "icon_plus") ?? UIImage()).withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
    
    lazy var mainViewStruct = MainViewStruct(
        viewBackgroundColor: .white,
        navBarBackgroundColor: mainColor,
        navBarTextColor: .white,
        labelTextColor: mainColor,
        cadastroCellStruct: CadastroCellStruct(
            placeholderTextField: "",
            keyboradTypeTextField: .default,
            cornerRadiusTextField: 12,
            borderColorTextField: mainColor.cgColor,
            borderWidthTextField: 0.4,
            autoCapitalizeTextField: .none,
            textColorTextField: mainColor,
            isSecuryEntry: false
        )
    )
    
    
    let cadastroCellStructs = [
        CadastroCellStruct(
            placeholderTextField: "Nome",
            keyboradTypeTextField: .default,
            cornerRadiusTextField: 12,
            borderColorTextField: UIColor.white.cgColor,
            borderWidthTextField: 0.4,
            autoCapitalizeTextField: .words,
            textColorTextField: UIColor.white,
            isSecuryEntry: false
        ),
        CadastroCellStruct(
            placeholderTextField: "E-mail",
            keyboradTypeTextField: .emailAddress,
            cornerRadiusTextField: 12,
            borderColorTextField: UIColor.white.cgColor,
            borderWidthTextField: 0.4,
            autoCapitalizeTextField: .none,
            textColorTextField: UIColor.white,
            isSecuryEntry: false
        ),
        CadastroCellStruct(
            placeholderTextField: "Senha",
            keyboradTypeTextField: .default,
            cornerRadiusTextField: 12,
            borderColorTextField: UIColor.white.cgColor,
            borderWidthTextField: 0.4,
            autoCapitalizeTextField: .words,
            textColorTextField: UIColor.white,
            isSecuryEntry: true
        ),
        
    ]
    
    let cadastroEditCellStructs = [
        CadastroCellStruct(
            placeholderTextField: "Título",
            keyboradTypeTextField: .default,
            cornerRadiusTextField: 12,
            borderColorTextField: UIColor.white.cgColor,
            borderWidthTextField: 0.4,
            autoCapitalizeTextField: .words,
            textColorTextField: UIColor.white,
            isSecuryEntry: false
        ),
        CadastroCellStruct(
            placeholderTextField: "Descrição breve",
            keyboradTypeTextField: .emailAddress,
            cornerRadiusTextField: 12,
            borderColorTextField: UIColor.white.cgColor,
            borderWidthTextField: 0.4,
            autoCapitalizeTextField: .none,
            textColorTextField: UIColor.white,
            isSecuryEntry: false
        ),
        CadastroCellStruct(
            placeholderTextField: "Detalhes da pauta",
            keyboradTypeTextField: .default,
            cornerRadiusTextField: 12,
            borderColorTextField: UIColor.white.cgColor,
            borderWidthTextField: 0.4,
            autoCapitalizeTextField: .none,
            textColorTextField: UIColor.white,
            isSecuryEntry: false
        ),
        
    ]
    
    func getLastPosition() -> Int32 {
        guard let position = UserDefaults.standard.value(forKey: "posicao") as? Int else {
            UserDefaults.standard.set(1, forKey: "posicao")
            UserDefaults.standard.synchronize()
            return 1
        }
        UserDefaults.standard.set((position + 1), forKey: "posicao")
        UserDefaults.standard.synchronize()
        return Int32(position) + 1
    }
    
}
